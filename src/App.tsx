import React, {Component} from "react";
import "./App.css";
import VideoPlayer from "./components/video/VideoPlayer";
import VideoPlayerOverlay from "./components/overlays/VideoPlayerOverlay";
import SidePanel from "./components/common/SidePanel";
import TabNavigator from "./components/common/TabNavigator";
import Playlist from "./components/controls/Playlist";
import Overlays from "./components/controls/Overlays";
import BusyBlip from "./components/controls/BusyBlip";
import {AppContext} from "./model/AppContext";

const context = new AppContext();

class App extends Component {

    render() {

        const videoOptions = {
            controls: true,
            fill: true,
            errorDisplay: false
        };

        return (
            <div className="App">
                <div className="App__playerContainer">
                    <VideoPlayer
                        options={videoOptions}
                        context={context}/>
                    <VideoPlayerOverlay context={context}/>
                </div>
                <SidePanel>
                    <TabNavigator>
                        <Playlist data-tab-label="Playlist" context={context}/>
                        <Overlays data-tab-label="Overlays" context={context}/>
                    </TabNavigator>
                </SidePanel>
                <BusyBlip context={context}/>
            </div>
        );
    }
}

export default App;