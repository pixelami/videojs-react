import ImageRenderer from "./ImageRenderer"
import TextRenderer from "./TextRenderer"
import IFrameRenderer from "./IFrameRenderer"
import {RenderPayload, RendererDescriptor} from "../../model/OverlayRenderer"
import VideoErrorModal from "./VideoErrorModal";

const map: { [k: string]: any } = {
    image: ImageRenderer,
    text: TextRenderer,
    iframe: IFrameRenderer,
    videoError: VideoErrorModal
};

export function createRenderDescriptor(payload: RenderPayload): RendererDescriptor {
    const $component = map[payload.renderer];
    return {component: $component, props: payload.props, id: payload.id};
}