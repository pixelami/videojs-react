import React, {Component, ReactNode} from "react";
import classNames from "classnames";
import "./OverlayContainer.css";

type Phase = "" | "enter" | "exit";
type HorizontalAlign = "center" | "left" | "right";

interface OverlayContainerProps {
    phase: Phase;
    onAnimationEnd: (evt:any) => void;
    animation: { enter: string, exit: string }
    verticalAlign: string;
    horizontalAlign: HorizontalAlign;
}

class OverlayContainer extends Component<OverlayContainerProps> {

    static defaultProps = {
        verticalAlign: 'bottom',
        horizontalAlign: 'center',
        animation: {enter: 'center', exit: 'center'},
        phase: ""
    };

    node:ReactNode;

    onAnimationEnd(evt: React.AnimationEvent) {
        if(this.props.onAnimationEnd && evt.target == this.node) {
            this.props.onAnimationEnd(evt);
        }
    }

    deriveAnimationClass() {
        if (!this.props.phase) return;
        return ['OverlayContainer--', this.props.phase, '-', this.props.verticalAlign,
            '-', this.props.animation[this.props.phase]].join("");
    }

    render() {
        const cls = classNames(
            'OverlayContainer',
            'OverlayContainer--align-' + this.props.verticalAlign,
            this.deriveAnimationClass());

        return (
            <div
                ref = { node => this.node = node}
                className={cls}
                style={{textAlign:this.props.horizontalAlign}}
                onAnimationEnd={(evt: any) => this.onAnimationEnd(evt)}>
                {this.props.children}
            </div>
        );
    }
}

export default OverlayContainer