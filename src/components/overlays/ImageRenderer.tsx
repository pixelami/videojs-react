import React, {Component} from "react";
import OverlayContainer from "./OverlayContainer";
import "./ImageRenderer.css";

type VerticalAlign = "top" | "middle" | "bottom";
type HorizontalAlign = "left" | "center" | "right";

interface StyleProps {
    [key: string]: any;
    verticalAlign: VerticalAlign;
    horizontalAlign: HorizontalAlign;
    animation:{enter:string, exit:string};
    url: string;
}

interface ImageRendererState {
    ready:boolean;
}

export class ImageRenderer extends Component<StyleProps, ImageRendererState> {

    constructor(props:any) {
        super(props);
        this.state = {ready:false};
    }

    onClick(evt: React.MouseEvent) {
        if (this.props.clickUrl) {
            window.open(this.props.clickUrl, '_blank');
        }
        this.props.deactivate();
    }

    onLoad() {
        console.log('loaded', this.props.url);
        this.setState({ready:true});
    }

    render() {
        const phase = this.state.ready ? this.props.active ? "enter" : "exit" : "";
        return (
            <OverlayContainer
                phase={phase}
                animation={this.props.animation}
                verticalAlign={this.props.verticalAlign}
                horizontalAlign={this.props.horizontalAlign}
                onAnimationEnd={this.props.onAnimationEnd}>
                <img className="ImageRenderer__img"
                     src={this.props.url}
                     onLoad={()=>this.onLoad()}
                     onClick={(evt:React.MouseEvent) => this.onClick(evt)}
                     alt={this.props.id}
                />
            </OverlayContainer>
        );
    }
}

export default ImageRenderer