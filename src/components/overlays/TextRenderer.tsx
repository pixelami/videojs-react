import React, {Component} from "react";
import OverlayContainer from "./OverlayContainer";

class TextRenderer extends Component<any> {
    render() {
        return (
            <OverlayContainer
                phase={this.props.active?"enter":"exit"}
                onAnimationEnd={this.props.onAnimationEnd}>
                {this.props.text}
            </OverlayContainer>
        );
    }
}

export default TextRenderer