import React, {Component} from "react";
import "./VideoPlayerOverlay.css";
import {observer} from "mobx-react";
import {AppContext} from "../../model/AppContext";
import {createRenderDescriptor} from "./OverlayRenderers";

interface VideoPlayerOverlayProps {
    context: AppContext;
}

@observer
class VideoPlayerOverlay extends Component<VideoPlayerOverlayProps> {

    render() {
        return (
            <div className="VideoPlayerOverlay">
                {this.props.context.overlayRenderer.content.map(data => {
                    const descriptor = createRenderDescriptor(data);
                    return <descriptor.component {...descriptor.props} key={descriptor.id}/>
                })}
            </div>
        );
    }
}

export default VideoPlayerOverlay;