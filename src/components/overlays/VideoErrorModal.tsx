import React, {Component} from "react";
import Stopwatch from "../common/Stopwatch";
import "./VideoErrorModal.css";
import OverlayContainer from "./OverlayContainer";

class VideoErrorModal extends Component<any> {
    render() {
        const phase = this.props.active ? "enter" : "exit";
        const contentClass = 'VideoErrorModal__content VideoErrorModal__content--' + phase;
        return (
            <OverlayContainer
                phase={phase}
                animation={this.props.animation}
                verticalAlign={this.props.verticalAlign}
                horizontalAlign={this.props.horizontalAlign}
                onAnimationEnd={this.props.onAnimationEnd}>
                <div className={contentClass}>
                    <h4>Oops</h4>
                    This video is currently unavailable.
                    <p><small>Next video starting...</small></p>
                    <Stopwatch width={32} height={32} duration={this.props.duration}/>
                </div>
            </OverlayContainer>
        );
    }
}

export default VideoErrorModal