import React, {Component} from "react";
import OverlayContainer from "./OverlayContainer";
import "./IFrameRenderer.css";

interface IFrameRendererState {
    contentReady: boolean;
}

class IFrameRenderer extends Component<any, IFrameRendererState> {

    iframeNode: any;

    constructor(props: any) {
        super(props);
        this.state = {contentReady: false};
    }

    onContentLoaded() {
        if (this.iframeNode.contentWindow) {
            window.addEventListener('message', (evt: MessageEvent) => {
                this.onIFrameMessage(evt);
            });

            this.iframeNode.contentWindow.postMessage("start", "*");
            this.setState({contentReady: true});
        } else {
            console.error("IFrame failed to load from", this.props.url);
        }
    }

    onIFrameMessage(evt:MessageEvent) {

        const source = evt.source as Window;
        if(source && source.location) {

            // validate the source of the message (in case there is more than one active overlay)
            const params = new URLSearchParams(source.location.search);
            if(params.has("id") && this.props.id == params.get("id")) {

                switch (evt.data) {
                    case 'ready':
                        // in theory we would only activate when the IFrame has told us
                        // it is ready. That means defining a messaging protocol for iframe
                        // content...
                        if(this.props.activate) this.props.activate();
                        break;
                    case 'complete':
                        if (this.props.deactivate) this.props.deactivate();
                        break;
                    default:
                        console.log("Received unknown data", evt.data);
                }
            }
        }

    }

    render() {

        const phase = this.state.contentReady ? this.props.active ? "enter" : "exit" : "";

        return (
            <OverlayContainer
                phase={phase}
                animation={this.props.animation}
                verticalAlign={this.props.verticalAlign}
                onAnimationEnd={this.props.onAnimationEnd}>
                <iframe
                    className="IFrameRenderer__iframe"
                    src={this.props.url + "?id=" + this.props.id}
                    ref={node => this.iframeNode = node}
                    onLoad={() => this.onContentLoaded()}
                />
            </OverlayContainer>
        );
    }
}

export default IFrameRenderer;