/// <reference path="./videojs-contrib-eme.d.ts" />
import React, {Component, ReactNode} from "react";
import "./VideoPlayer.css"
import videojs from "video.js"
import {AppContext} from "../../model/AppContext";
import {AudioTrackListVjs} from "./AudioTrackList";
import {observer} from "mobx-react";
import * as eme from "videojs-contrib-eme";

// register the AudioTrackList component with videojs
AudioTrackListVjs.register();

interface VideoPlayerProps {
    options: videojs.PlayerOptions;
    context: AppContext;
}

@observer
class VideoPlayer extends Component<VideoPlayerProps> {

    videoNode: ReactNode;

    constructor(props: VideoPlayerProps) {
        super(props);
    }

    componentDidMount() {

        const player = videojs(this.videoNode, this.props.options, () => {
            //console.log('onPlayerReady', player);
            this.props.context.playerState.setPlayer(player);
            this.configurePlayer(player);
        });
    }

    configurePlayer(player:any) {
        const controlBar = player.getChild('controlBar') as videojs.ControlBar;
        if (controlBar) controlBar.addChild('AudioTrackListVjs', {});

        try {
            player.eme();
        } catch (e) {
            console.error("unable to initialize eme plugin");
        }
    }

    componentWillUnmount() {
        this.props.context.playerState.unsetPlayer();
    }

    render() {

        const style:any = {
            'visibility':this.props.context.playerState.contentReadyToPlay?'visible':'hidden'
        };

        return (
            <div className="VideoPlayer__container" style={style}>

                <div data-vjs-player>
                    <video
                        ref={node => this.videoNode = node}
                        className="video-js vjs-big-play-centered">
                    </video>
                </div>
            </div>
        )
    }
}

// force eme to be included.
const _eme = eme;

// override error message
videojs.addLanguage('en', {"The media could not be loaded, either because the server or network failed or because the format is not supported.": "Oops, This video is currently unavailable."});

export default VideoPlayer;