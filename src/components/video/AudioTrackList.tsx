import React, {Component} from "react";
import videojs from "video.js";
import ReactDOM from "react-dom";
import OptionSelector from "../common/OptionSelector";
import "./AudioTrackList.css";
import {Headphone} from "../common/Icons";

interface AudioTrackListProps {
    videoComponent: videojs.Component;
}

interface AudioTrackListState {
    audioTracks: any[],
    selectedAudioTrack: any
}

class AudioTrackList extends Component<AudioTrackListProps, AudioTrackListState> {

    constructor(props: AudioTrackListProps) {
        super(props);
        this.state = {
            audioTracks: [],
            selectedAudioTrack: null
        }
    }

    componentDidMount(): void {
        const player = this.props.videoComponent.player();
        player.on('loadeddata', ()=>this.loadedDataHandler());
    }

    loadedDataHandler() {
        const player = this.props.videoComponent.player();
        const audioTracks = audioTracksToArray(player.audioTracks());
        const selectedAudioTrack = audioTracks.find(v => v.enabled == true);
        this.setState({
            audioTracks: audioTracks,
            selectedAudioTrack: selectedAudioTrack
        });
    };

    componentWillUnmount(): void {
        const player = this.props.videoComponent.player();
        player.off('loadeddata', this.loadedDataHandler);
    }

    onChange(data: any) {
        if (this.state.selectedAudioTrack && this.state.selectedAudioTrack.enabled) {
            this.state.selectedAudioTrack.enabled = false;
        }
        data.enabled = true;
        this.setState({selectedAudioTrack: data});
    }

    render() {
        return (
            <div className="AudioTrackList">
                <OptionSelector
                    options={this.state.audioTracks}
                    selected={this.state.selectedAudioTrack}
                    onChange={(data) => this.onChange(data)}
                    placeHolder={<Headphone/>}
                    placement="top"
                    mode="menu"
                >
                </OptionSelector>
            </div>
        );
    }
}

const VideoComponent = videojs.getComponent('Component');

export class AudioTrackListVjs extends VideoComponent {

    static register() {
        videojs.registerComponent('AudioTrackListVjs', AudioTrackListVjs);
    }

    constructor(player: any, options: any) {
        super(player, options);
        player.ready(() => this.mount());
        /* Remove React root when component is destroyed */
        this.on("dispose", () => ReactDOM.unmountComponentAtNode(this.el()));
    }

    mount() {
        this.el().classList.add('vjs-control', 'vjs-button');
        ReactDOM.render(<AudioTrackList videoComponent={this}/>, this.el());
    }
}

function audioTracksToArray(audioTracks: any): any[] {
    const arr: any[] = [];
    if (!audioTracks) return arr;
    for (let i = 0; i < audioTracks.length; i++) {
        arr.push(audioTracks[i]);
    }
    return arr;
}

export default AudioTrackList;