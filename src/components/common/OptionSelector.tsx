import React, {Component} from "react";
import "./OptionSelector.css";
import classNames from "classnames";
import Popper from "popper.js";

type OptionSelectorMode = "select" | "menu";

interface OptionSelectorProps {
    options: any[];
    selected?: any,
    placeHolder?: string | React.ReactElement;
    labelField: string;
    onChange: (data: any) => void;
    placement: Popper.Placement;
    mode: OptionSelectorMode;
}

interface OptionSelectorState {
    showOptions: boolean;
    popperData: Popper.Data;
}

class OptionSelector extends Component<OptionSelectorProps, OptionSelectorState> {

    static defaultProps = {
        labelField: "label",
        placeHolder: "choose",
        placement: "bottom-start",
        mode: "select"
    };

    buttonNode: any;
    contentNode: any;
    popper: Popper | undefined;

    constructor(props: OptionSelectorProps) {
        super(props);
        this.state = {
            showOptions: false,
            popperData: {} as Popper.Data
        };
    }

    private onItemClick(data: any, evt: React.MouseEvent) {
        this.setState({showOptions: false});
        this.props.onChange(data);
    }

    private getLabel(data: any, i = 0) {
        if (typeof data == "string") return data;
        return data[this.props.labelField] || "item " + i;
    }

    private getButtonLabel() {
        if (this.props.mode == "menu" || !this.props.selected) return this.props.placeHolder;
        return this.getLabel(this.props.selected);
    }

    private onButtonClick(evt: React.MouseEvent) {
        this.setState({showOptions: !this.state.showOptions});
        if(this.popper) this.popper.scheduleUpdate();
    }

    private onMouseLeave(evt: React.MouseEvent) {
        this.setState({showOptions: false});
        if(this.popper) this.popper.scheduleUpdate();
    }

    private deriveContentStyle() {
        const pos = {x: 0, y: 0, width: 'auto'};
        if (this.state.popperData.offsets) {
            pos.x = this.state.popperData.offsets.popper.left;
            pos.y = this.state.popperData.offsets.popper.top;
            const ref_width = this.state.popperData.offsets.reference.width;
            const pop_width = this.state.popperData.offsets.popper.width;
            pos.width = pop_width < ref_width ? ref_width + 'px' : pop_width + 'px';
        }

        return {
            transform: 'translate3d(' + pos.x + 'px, ' + pos.y + 'px, 0)',
            position: 'absolute',
            width: 'auto',
            minWidth: pos.width,
            top: 0,
            left: 0
        } as any;
    }

    componentDidMount(): void {

        if (!this.contentNode || !this.buttonNode) return;
        const options = derivePopperOptions(this.props);
        options.onUpdate = (data) => {
            this.setState({popperData: data});
        };
        this.popper = new Popper(this.buttonNode, this.contentNode, options);
        this.popper.enableEventListeners();
        //this.popper.scheduleUpdate();
    }

    render() {

        const list = this.renderList();

        const contentClass = classNames(
            'OptionSelector__content',
            {'OptionSelector__content--show': list && this.state.showOptions});


        return (

            <div className="OptionSelector"
                 onMouseLeave={(evt: React.MouseEvent) => this.onMouseLeave(evt)}>
                <div
                    className={contentClass}
                    style={this.deriveContentStyle()}
                    ref={node => this.contentNode = node}>
                    {list}
                </div>
                <button
                    className="OptionSelector__button"
                    ref={node => this.buttonNode = node}
                    onClick={(evt: React.MouseEvent) => this.onButtonClick(evt)}>
                    {this.getButtonLabel()}
                </button>
            </div>
        );
    }

    renderList() {

        if (!this.props.options) return;

        let options = this.props.options;
        if (this.props.mode == "select" && this.props.selected) {
            options = options.filter(v => v != this.props.selected)
        }

        return (
            <ul className="OptionSelector__list">
                {options.map((v, i) =>
                    <OptionSelectorItem
                        key={i}
                        data={v}
                        selected={v == this.props.selected}
                        onItemClick={(data: any, evt: React.MouseEvent) => this.onItemClick(data, evt)}
                        getLabel={(data: any) => this.getLabel(data, i)}
                    />
                )}
            </ul>
        );
    }
}

interface OptionSelectorItemProps {
    data: any;
    getLabel: (data: any) => string;
    onItemClick: (data: any, evt: React.MouseEvent) => void;
    selected: boolean;
}

class OptionSelectorItem extends Component<OptionSelectorItemProps> {

    onItemClick(evt: React.MouseEvent) {
        evt.preventDefault();
        this.props.onItemClick(this.props.data, evt);
    }

    render() {
        const cls = classNames({
            'OptionSelectorItem': true,
            'OptionSelectorItem--selected': this.props.selected
        });
        return (
            <li onClick={(evt: React.MouseEvent) => this.onItemClick(evt)}
                className={cls}>
                {this.props.getLabel(this.props.data)}
            </li>
        );
    }
}

export default OptionSelector


const derivePopperOptions = (props: OptionSelectorProps): Popper.PopperOptions => {
    return {
        placement: props.placement,
        positionFixed: true,
        modifiersIgnored: ['applyStyle']
    } as Popper.PopperOptions;
};