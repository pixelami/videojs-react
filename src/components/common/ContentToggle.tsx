import React, {Component} from "react";
import classNames from "classnames";
import ChevronButton from "./ChevronButton";
import "./ContentToggle.css";

interface ContentToggleProps {
    title:string;
    indent:boolean;
}

interface ContentToggleState {
    show:boolean;
}

class ContentToggle extends Component<ContentToggleProps, ContentToggleState> {

    static defaultProps = {
        indent: true
    };

    constructor(props:ContentToggleProps) {
        super(props);
        this.state = {show:false};
    }

    render() {

        const contentClass = classNames('ContentToggle__content', {
            'ContentToggle__content--show': this.state.show,
            'ContentToggle__content--indent': this.props.indent
        });

        return (
            <div className="ContentToggle">
                <div className="ContentToggle__header">
                    <ChevronButton
                        orientation={this.state.show?"down":"right"}
                        onClick={()=>this.setState(state => ({show:!state.show}))}/>
                    <div className="ContentToggle__title">{this.props.title}</div>
                </div>
                <div className={contentClass}>
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default ContentToggle