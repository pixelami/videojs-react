import React, {Component} from "react";
import "./ChevronButton.css";
import classNames from "classnames";
import Chevron, {ChevronOrientation} from "./Chevron";

type Size = "" | "sm";

interface ChevronButtonProps {
    orientation: ChevronOrientation;
    onClick: (orientation: ChevronOrientation) => void;
    size:Size;
}

class ChevronButton extends Component<ChevronButtonProps> {

    static defaultProps = {
        orientation: "right",
        size:""
    };

    onClick() {
        this.props.onClick(this.props.orientation);
    }

    render() {
        const cls = classNames('ChevronButton', {
            'ChevronButton--sm':this.props.size == 'sm'
        });
        return (
            <button className={cls} onClick={() => this.onClick()}>
                <Chevron orientation={this.props.orientation} animate={true}/>
            </button>
        );
    }
}

export default ChevronButton