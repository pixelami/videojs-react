import React from "react";

interface IconProps {
    width: number;
    height: number;
}

export const MovieStrip = (props: IconProps) => (
    <svg version="1.1"
         width={props.width} height={props.height}
         viewBox="0 0 55.125 55.125">
        <g>
            <g>
                <path fill="currentColor" d="M4.167,47.007h46.792c2.302,0,4.167-1.866,4.167-4.167V12.285c0-2.301-1.865-4.167-4.167-4.167H4.167
			C1.866,8.118,0,9.984,0,12.285V42.84C0,45.141,1.866,47.007,4.167,47.007z M30.771,44.082h-6.418v-3.965h6.418V44.082z
			 M46.065,11.138h6.418v3.965h-6.418V11.138z M45.875,40.117h6.419v3.965h-6.419V40.117z M35.303,11.138h6.419v3.965h-6.419V11.138
			z M35.114,40.117h6.419v3.965h-6.419V40.117z M24.543,11.138h6.418v3.965h-6.418V11.138z M22.743,20.717l9.639,4.82
			c2.237,1.119,2.237,2.933,0,4.051l-9.639,4.821c-2.237,1.118-4.052-0.003-4.052-2.506v-8.681
			C18.69,20.72,20.505,19.599,22.743,20.717z M13.782,11.138h6.419v3.965h-6.419V11.138z M13.593,40.117h6.418v3.965h-6.418V40.117z
			 M3.021,11.138h6.419v3.965H3.021V11.138z M2.832,40.117h6.419v3.965H2.832V40.117z"/>
            </g>
        </g>
    </svg>
);

MovieStrip.defaultProps = {width:48, height:48};

export const Headphone = (props: IconProps) => (
    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="headphones-alt"
         className="svg-inline--fa fa-headphones-alt fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg"
         viewBox="0 0 512 512" width={props.width} height={props.height}>
        <path fill="currentColor"
              d="M160 288h-16c-35.35 0-64 28.7-64 64.12v63.76c0 35.41
              28.65 64.12 64 64.12h16c17.67 0 32-14.36
              32-32.06V320.06c0-17.71-14.33-32.06-32-32.06zm208
              0h-16c-17.67 0-32 14.35-32 32.06v127.88c0 17.7 14.33 32.06 32
              32.06h16c35.35 0 64-28.71 64-64.12v-63.76c0-35.41-28.65-64.12-64-64.12zM256
              32C112.91 32 4.57 151.13 0 288v112c0 8.84 7.16 16 16 16h16c8.84 0
              16-7.16 16-16V288c0-114.67 93.33-207.8 208-207.82 114.67.02 208 93.15
              208 207.82v112c0 8.84 7.16 16 16 16h16c8.84 0 16-7.16 16-16V288C507.43
              151.13 399.09 32 256 32z"></path>
    </svg>
);

Headphone.defaultProps = {width: 16, height: 16};