import React, {Component} from "react";
import "./TabNavigator.css";
import classNames from "classnames";


const DATA_TAB_LABEL_PROPERTY = 'data-tab-label';

interface TabNavigatorProps {
    tabLabelProperty:string;
}

interface TabNavigatorState {
    tabIndex: any;
    tabLabels: string[];
    selectedTab: string;
}

class TabNavigator extends Component<TabNavigatorProps, TabNavigatorState> {

    static defaultProps = {
        tabLabelProperty:DATA_TAB_LABEL_PROPERTY
    };

    constructor(props: any) {
        super(props);
        const labels = this.getTabLabels();
        this.state = {
            tabIndex: this.buildTabIndex(),
            tabLabels: labels,
            selectedTab: labels[0]
        }
    }

    getTabLabels() {
        return React.Children.map(this.props.children,
            (node: any, i: number) => this.getNodeLabel(node, i));
    }

    getNodeLabel(node: any, index: number) {
        return node.props[this.props.tabLabelProperty] || 'Tab ' + index;
    }

    buildTabIndex() {
        const tabIndex: any = {};
        React.Children.forEach(this.props.children, (node: any, i) => {
            const label = this.getNodeLabel(node, i);
            tabIndex[label] = node;
        });
        return tabIndex;
    }

    onTabSelected(tabIndex: string) {
        this.setState({selectedTab: tabIndex});
    }

    resolveTabNodeByLabel(label: string) {
        return this.state.tabIndex[label];
    }

    render() {
        const navBar = this.renderNavBar();
        return <div className="TabNavigator">
            {navBar}
            <div className="TabNavigator__contentContainer">
                {this.resolveTabNodeByLabel(this.state.selectedTab)}
            </div>
        </div>;
    }

    renderNavBar() {

        const tabs = this.state.tabLabels.map((label, i) =>
            <Tab
                selected={this.state.selectedTab == label}
                onTabSelected={(tabIndex: string) => this.onTabSelected(tabIndex)}
                label={label}
                key={i}
            />
        );

        return (
            <ul className="TabNavigator__navBar">
                {tabs}
            </ul>
        );
    }
}

interface TabProps {
    selected: boolean;
    onTabSelected: (label: string) => void;
    label: string;
}

class Tab extends Component<TabProps> {

    render() {
        const tabClass = classNames('Tab', {
            'Tab--selected': this.props.selected
        });

        const btnClass = classNames('Tab__button', {
            'Tab__button--selected btn-primary': this.props.selected,
            'btn-secondary': !this.props.selected
        });

        return (
            <li className={tabClass}>
                <button
                    className={btnClass}
                    onClick={() => this.props.onTabSelected(this.props.label)}>
                    {this.props.label}
                </button>
            </li>
        );
    }
}

export default TabNavigator