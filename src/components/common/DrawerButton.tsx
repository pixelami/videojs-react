import React, {Component} from "react";
import "./DrawerButton.css";
import Chevron, {ChevronOrientation} from "./Chevron";

interface DrawerButtonProps {
    open:boolean;
    states:ChevronOrientation[]
    onClick: ((evt: any) => void);
}

class DrawerButton extends Component<DrawerButtonProps> {

    static defaultProps = {open:false, states:["left", "right"]};

    onClick(evt: React.MouseEvent) {
        if (this.props.onClick) this.props.onClick(evt);
    }

    deriveOrientation() {
        return this.props.open?this.props.states[1]:this.props.states[0]
    }

    render() {

        return (
            <button
                className="DrawerButton btn-primary"
                onClick={(evt: React.MouseEvent) => this.onClick(evt)}>
                <Chevron orientation={this.deriveOrientation()} animate={true}/>
            </button>
        )
    }
}

export default DrawerButton