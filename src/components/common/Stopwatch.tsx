import React, {Component} from "react";
import "./Stopwatch.css";

interface StopwatchProps {
    width:number;
    height:number;
    duration:number;
}

class Stopwatch extends Component<StopwatchProps> {
    render() {
        return <svg className="Stopwatch" viewBox="-64 -64 128 128"
                    width={this.props.width} height={this.props.height}>
            <circle
                className="Stopwatch__rim"
                stroke="currentColor"
                fill="none"
                cx="0" cy="0" r="56"/>
            <line
                stroke="currentColor"
                fill="none"
                className="Stopwatch__arm"
                style={{animationDuration: this.props.duration + 'ms'}}
                x1="0" y1="0" y2="-44" x2="0"/>
        </svg>;
    }
}

export default Stopwatch