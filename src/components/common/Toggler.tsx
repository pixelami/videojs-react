import React, {Component} from "react";
import "./Toggler.css";

let idCounter = 1;

interface TogglerProps {
    on:boolean;
    onToggle:()=>void;
}

class Toggler extends Component<TogglerProps> {

    id = 'Toggler-'+(idCounter++);

    onChanged() {
        if(this.props.onToggle) this.props.onToggle();
    }

    render() {
        return (
            <div className="slideThree">
                <input type="checkbox"
                       value="None"
                       id={this.id}
                       name="check"
                       checked={this.props.on}
                       onChange={()=>this.onChanged()}/>
                <label
                    htmlFor={this.id}/>
            </div>
        );
    }
}

export default Toggler