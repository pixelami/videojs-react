import React, {Component} from "react";
import classNames from "classnames";
import "./Chevron.css";

export type ChevronOrientation = "left" | "right" | "up" | "down" | number;

interface ChevronProps {
    orientation:ChevronOrientation;
    animate?:boolean;
}

class Chevron extends Component<ChevronProps> {

    static defaultProps = {orientation:"right", animate:false};

    resolveClassModifier() {
        if (typeof this.props.orientation == 'string')
            return 'Chevron--' + this.props.orientation;
    }


    render() {
        const iconClass = classNames('Chevron', this.resolveClassModifier(),
            {'Chevron--animate':this.props.animate});

        return (
            <svg className={iconClass}
                 aria-hidden="true"
                 focusable="false"
                 role="img"
                 xmlns="http://www.w3.org/2000/svg"
                 viewBox="0 0 256 512">
                <path
                    fill="currentColor"
                    d="M31.7 239l136-136c9.4-9.4 24.6-9.4 33.9 0l22.6 22.6c9.4 9.4 9.4 24.6 0 33.9L127.9 256l96.4 96.4c9.4 9.4 9.4 24.6 0 33.9L201.7 409c-9.4 9.4-24.6 9.4-33.9 0l-136-136c-9.5-9.4-9.5-24.6-.1-34z"></path>
            </svg>
        )
    }
}

export default Chevron