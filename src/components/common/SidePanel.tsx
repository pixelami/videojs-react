import React, {Component} from "react";
import "./SidePanel.css";
import DrawerButton from "./DrawerButton";
import classNames from "classnames";

interface SidePanelState {
    show: boolean
}

class SidePanel extends Component<any, SidePanelState> {

    constructor(props:any) {
        super(props);
        this.state = {show: false};
    }

    onClick() {
        this.setState({show: !this.state.show});
    }

    render() {
        const cls = classNames('SidePanel', {'SidePanel--show':this.state.show});

        return (
            <div className={cls}>
                <div className="SidePanel__content">
                    {this.props.children}
                </div>
                <DrawerButton
                    onClick={() => this.onClick()}
                    open={this.state.show}/>
            </div>
        );
    }
}

export default SidePanel;