import React, {Component, CSSProperties} from "react";
import classNames from "classnames";
import "./Pill.css";

type PillType = "none" | "primary" | "secondary"
    | "tertiary" | "quaternary" | "quinary"
    | "senary" | "septenary" | "octonary"
    | "nonary" | "denary";

type Size = "" | "sm" | "lg"

interface PillProps {
    type:PillType;
    size:Size;
    style?:CSSProperties;
}

class Pill extends Component<PillProps> {

    static defaultProps = {
        type:"primary",
        size:"sm"
    };

    render() {
        const cls = classNames('Pill', {
            ['Pill--'+this.props.type] : this.props.type != "none",
            ['Pill--'+this.props.size] : this.props.size != "",
            'Pill--secondary' : !this.props.type
        });
        return <span className={cls} style={this.props.style}>{this.props.children}</span>;
    }
}

export default Pill;