import React, {Component} from "react";
import {observer} from "mobx-react";
import {AppContext} from "../../model/AppContext";
import classNames from "classnames";
import ChevronButton from "../common/ChevronButton";
import OverlayOptions from "./OverlayOptions";
import Toggler from "../common/Toggler";
import "./Overlays.css";
import Pill from "../common/Pill";

interface OverlaysProps {
    context: AppContext;
}

@observer
class Overlays extends Component<OverlaysProps> {

    constructor(props: OverlaysProps) {
        super(props);
    }

    onOverlayChange(data: {active:boolean, payload:any}) {
        //console.log('overlayRenderer change', data);
        if (data.active) {
            this.props.context.overlayRenderer.executeAction("RENDER", data.payload)
        } else {
            this.props.context.overlayRenderer.executeAction("CLEAR", {id: data.payload.id})
        }
    }


    render() {
        return (
            <ul className="Overlays">
                {this.props.context.overlay.overlays.map((overlay: any, i) =>
                    <OverlayItem
                        key={i}
                        title={overlay.title}
                        onChange={(data) => this.onOverlayChange(data)}
                        data={overlay}/>
                )}
            </ul>
        );
    }
}

interface OverlayItemProps {
    title: string;
    onChange: (data: { active: boolean, payload: any }) => void;
    data: any;
}

interface OverlayItemState {
    active: boolean;
    showExtra: boolean;
}

class OverlayItem extends Component<OverlayItemProps, OverlayItemState> {

    constructor(props: OverlayItemProps) {
        super(props);
        this.state = {
            active: false,
            showExtra: false
        }
    }

    onClick(evt?: React.MouseEvent) {

        const active = !this.state.active;

        this.setState((state) => {
            return {active: active}
        });

        this.props.onChange({
            active: active,
            payload: this.props.data
        });
    }

    render() {

        const extraClass = classNames('OverlayItem__extra',
            {'OverlayItem__extra--show': this.state.showExtra});

        return (
            <li className="OverlayItem">
                <div className="OverlayItem__main">
                    <ChevronButton
                        orientation={this.state.showExtra?"down":"right"}
                        size="sm"
                        onClick={()=>this.setState(state =>({showExtra:!state.showExtra}))}/>
                    <div className="Overlay__titleContainer">
                        <div className="OverlayItem__title">{this.props.title}</div>
                        <div className="OverlayItem__subTitle">
                            <Pill type={rendererToClassMap[this.props.data.renderer]}
                                  size="sm"
                                  style={{marginLeft:0}}>{this.props.data.renderer}</Pill>
                        </div>
                    </div>

                    <Toggler on={this.state.active} onToggle={()=>this.onClick()}/>
                </div>
                <div className={extraClass}>
                    <OverlayOptions data={this.props.data} show={this.state.showExtra}/>
                </div>
            </li>
        );
    }
}

const rendererToClassMap:any = {
    image: 'quaternary',
    iframe: 'tertiary'
};

export default Overlays