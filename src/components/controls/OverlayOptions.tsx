import React, {Component} from "react";
import OptionSelector from "../common/OptionSelector";
import "./OverlayOptions.css";
import {observer} from "mobx-react";
import classNames from "classnames";

type VerticalAlign = "top" | "bottom";
type HorizontalAlign = "left" | "center" | "right";

interface DirectionOptions {
    top: HorizontalAlign[]
    bottom: HorizontalAlign[]
}

const verticalAlignOptions: VerticalAlign[] = ["top", "bottom"];
const horizontalAlignOptions = ["left", "center", "right"];
const directionOptions: DirectionOptions = {
    top: ["left", "center", "right"],
    bottom: ["left", "center", "right"]
};

interface OverlayStyleProperties {
    [key: string]: any;
    verticalAlign: VerticalAlign;
    horizontalAlign: HorizontalAlign;
    animation: { enter: string, exit: String };
}

interface OverlayOptionsProps {
    data: { props: OverlayStyleProperties };
    onChange?: (propertyName: string, value: any) => void;
    show:boolean;
}

@observer
class OverlayOptions extends Component<OverlayOptionsProps> {

    onChange(property: string, value: string) {
        let ref = this.props.data.props;
        let path = property.split(".");
        while (path.length > 1) ref = ref[path.shift() as string];
        ref[path[0]] = value;
    }

    render() {

        const cls = classNames('OverlayOptions',{'OverlayOptions--show':this.props.show});

        return (
            <div className={cls}>
                <div className="OverlayOptions__group">
                    <div className="OverlayOptions__label">horizontal</div>
                    <OptionSelector options={horizontalAlignOptions}
                                    selected={this.props.data.props.horizontalAlign}
                                    onChange={(value) => this.onChange('horizontalAlign', value)}/>
                </div>
                <div className="OverlayOptions__group">
                    <div className="OverlayOptions__label">vertical</div>
                    <OptionSelector options={verticalAlignOptions}
                                    selected={this.props.data.props.verticalAlign}
                                    onChange={(value) => this.onChange('verticalAlign', value)}/>
                </div>
                <div className="OverlayOptions__group">
                    <div className="OverlayOptions__label">enter</div>
                    <OptionSelector options={directionOptions[this.props.data.props.verticalAlign]}
                                    selected={this.props.data.props.animation.enter}
                                    onChange={(value) => this.onChange('animation.enter', value)}
                    />
                </div>
                <div className="OverlayOptions__group">
                    <div className="OverlayOptions__label">exit</div>
                    <OptionSelector options={directionOptions[this.props.data.props.verticalAlign]}
                                    selected={this.props.data.props.animation.exit}
                                    onChange={(value) => this.onChange('animation.exit', value)}/>
                </div>
            </div>
        )
    }
}

export default OverlayOptions