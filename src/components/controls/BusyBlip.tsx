import React, {Component} from "react";
import "./BusyBlip.css";
import {observer} from "mobx-react";
import {AppContext} from "../../model/AppContext";

@observer
class BusyBlip extends Component<{context:AppContext}> {
    render() {
        return (
            <div className="BusyBlip" style={{display:this.props.context.playerState.contentReadyToPlay?'none':'flex'}}>
                <svg width="128px" height="128px" viewBox="0 0 280 400"
                     xmlns="http://www.w3.org/2000/svg" version="1.1">

                    <g transform="translate(40, 200)">
                        <rect className="blip b1" width="40" height="40"></rect>
                    </g>
                    <g transform="translate(120,200)">
                        <rect className="blip b2" width="40" height="40"></rect>
                    </g>
                    <g className="blip" transform="translate(200,200)">
                        <rect className="blip b3" width="40" height="40"></rect>
                    </g>
                </svg>
            </div>
        )
    }
}

export default BusyBlip