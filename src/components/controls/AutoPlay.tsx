import React, {Component} from 'react'
import Toggler from "../common/Toggler";
import "./AutoPlay.css";
import {AppContext} from "../../model/AppContext";
import {observer} from "mobx-react";

interface AutoPlayProps {
    store:AppContext;
}

@observer
class AutoPlay extends Component<AutoPlayProps> {
    render() {
        return (<div className="AutoPlay">
            <div className="AutoPlay__label">autoplay</div>
            <Toggler on={this.props.store.playerState.autoPlay}
                     onToggle={()=>this.props.store.playerState.toggleAutoPlay()}/>
        </div>);
    }
}

export default AutoPlay