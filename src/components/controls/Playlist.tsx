import React, {Component} from "react"
import {observer} from "mobx-react"
import {AppContext} from "../../model/AppContext"
import classNames from "classnames"
import "./Playlist.css"
import AutoPlay from "./AutoPlay";
import Pill from "../common/Pill";
import {MovieStrip} from "../common/Icons";

interface PlaylistProps {
    context: AppContext;
}

@observer
class Playlist extends Component<PlaylistProps> {

    playlistItemSelected(data:any) {
        this.props.context.playerState.setCurrent(data);
    }

    render() {

        return (
            <div className="Playlist">
                <AutoPlay store={this.props.context}/>
                <div className="Playlist__scrollContent">
                    <ul className="Playlist__list">
                        {this.props.context.playlist.items.map((item, i) =>
                            <PlaylistItem
                                data={item}
                                key={i}
                                inPlay={i==this.props.context.playlist.index}
                                onClick={(data) => this.playlistItemSelected(data)}/>
                        )}
                    </ul>
                </div>
            </div>
        );
    }
}

interface PlaylistItemProps {
    data: {
        url: string;
        type: string;
        thumb: string;
        title: string;
        subtitle: string;
        options: any;
    }
    onClick: (data: any) => void;
    inPlay: boolean;
}

class PlaylistItem extends Component<PlaylistItemProps> {

    render() {

        const cls = classNames('PlaylistItem', {'PlaylistItem--inPlay': this.props.inPlay});
        const typeLabel = resolveTypeLabel(this.props.data.type);
        const pillModifier = labelToModifierMap[typeLabel] || "secondary";
        const thumb = this.renderThumb();

        return (
            <li className={cls}
                onClick={() => this.props.onClick(this.props.data)}>
                {thumb}
                <div className="PlaylistItem__main">
                    <div className="Playlist__title">{this.props.data.title}</div>
                    <div className="Playlist__subTitle">
                        <span>{this.props.data.subtitle}</span>
                        <Pill type={pillModifier} size={"sm"}>{typeLabel}</Pill>
                    </div>
                </div>
            </li>
        );
    }

    renderThumb() {
        if(this.props.data.thumb) {
            return (
                <img className="PlaylistItem__image"
                     alt={this.props.data.title}
                     src={this.props.data.thumb}/>
            )
        }

        return (
            <MovieStrip width={48} height={48}/>
        )
    }
}

const mimeTypeToLabelMap:any = {
    'application/x-mpegURL':'m3u8',
    'application/dash+xml': 'mpd'
};

const resolveTypeLabel = (type:string) => {
    let label = mimeTypeToLabelMap[type];
    if(label) return label;
    label = type.split("/");
    if(label.length > 1) return label[1];
    return label[0];
};

const labelToModifierMap:any = {
    mp4:"tertiary",
    webm:"quaternary",
    mpd:"quinary",
    m3u8:"senary"
};

export default Playlist