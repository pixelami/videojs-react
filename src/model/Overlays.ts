import data from "./data/overlay-data";
import {observable} from "mobx";

export interface OverlayData {
    title:string;
    renderer:string;
    id:String;
    props:any;
}

export class Overlays {

    @observable overlays:OverlayData[] = [];

    constructor() {
        this.overlays.push(
            data.images[2].payload,
            data.images[1].payload,
            data.images[0].payload,
            data.iframes[0].payload,
            data.iframes[1].payload
        );
    }
}
