import {Timeline} from "./";
import {Cue, CueStatus} from "./CuePoints";



it("instantiates without error", ()=>{
    const timeline = new Timeline();
    expect(timeline).toBeInstanceOf(Timeline);
});

describe("Timeline", () => {

    //jest.useFakeTimers();
    it("fires cues when timeline is updated", done => {
        const timeline = new Timeline();
        timeline.addCueByProps(5000, "show_message");
        timeline.subscribe((cue:Cue)=>{
            console.log("subscriber called");
            expect(cue.status).toBe(CueStatus.Fired);
            done();
        });

        // calling updateTime should cause the added cue to be armed
        timeline.updateTime(4850);

        // calling armImmediatelyPendingCues should not return anything
        // since all pending cues should have been armed when calling
        // `updateTime`
        const cues = timeline.armAndReturnImmediatelyPendingCues();
        expect(cues).toHaveLength(0);
    });




});