import {observable} from "mobx";

export class CuePoints {

    @observable cues:Cue[] = [];

    addCue(cue:Cue) {
        this.cues.push(cue);
        this.cues = this.cues.slice().sort((a,b)=>a.time - b.time);
    }

    remove(cue:Cue) {
        this.removeById(cue.id);
    }

    removeById(id:number) {
        let idx = this.cues.findIndex(v=>v.id === id);
        if(idx > -1) this.cues.splice(idx,1);
    }

    getPendingCuesBetween(start_ms:number, end_ms:number):Cue[] {
        let i = 0,
            cue:Cue,
            cues:Cue[] = [];

        while(i < this.cues.length) {
            cue = this.cues[i++];
            // because cues are always time sorted we can bail early
            if(cue.time > end_ms) break;
            if(cue.time < start_ms) continue;
            if(cue.status === CueStatus.Pending) cues.push(cue);
        }
        return cues;
    }

    getPendingCuesWithActionBefore(action:string = "*", time:number):Cue[] {
        let i = 0,
            cue:Cue,
            cues:Cue[] = [];

        while(i < this.cues.length) {
            cue = this.cues[i++];
            // because cues are always time sorted we can bail early
            if(cue.time > time) break;
            if(action == "*") {
                if(cue.status === CueStatus.Pending) cues.push(cue);
            } else {
                if(cue.status === CueStatus.Pending && cue.action == action) cues.push(cue);
            }
        }
        return cues;
    }

    getCuesBefore(time:number) {
        let i = 0,
            cue:Cue,
            cues:Cue[] = [];

        while(i < this.cues.length) {
            cue = this.cues[i++];
            // because cues are always time sorted we can bail early
            if (cue.time > time) break;
            cues.push(cue);
        }
        return cues;
    }
}

export enum CueStatus {
    Pending, Armed, Fired
}

export class ValidationError extends Error {
    errors:any[] = [];
    constructor(errors:any[]) {
        super("ValidationError");
        this.errors = errors;
    }
}

export class Cue<T = any> {

    static validateOptions(opts:any) {
        const errors:any[] = [];
        const required = ['time', 'action'];
        required.forEach(key=>{
            if(!opts.hasOwnProperty(key)) errors.push(`requires '${key}' field`);
        });
        return errors;
    }

    static fromJson(json:string) {
        let data;
        try {
            data = JSON.parse(json);
        } catch(e) {
            throw new ValidationError([e]);
        }

        const errors = Cue.validateOptions(data);
        if(errors.length > 0) throw new ValidationError(errors);
        return new Cue(data.action, data.time, data.payload);
    }

    static COUNT = 1;
    id:number;
    time:number;
    action:string;
    payload:T;
    status = CueStatus.Pending;
    timeoutId:any;

    constructor(action:string, time:number, payload?:any) {
        this.id = Cue.COUNT++;
        this.action = action;
        this.time = time;
        this.payload = payload;
    }
}