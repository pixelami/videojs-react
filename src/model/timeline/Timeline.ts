import {Cue, CuePoints, CueStatus} from "./CuePoints";
import {observable} from "mobx";
import {CLEAR} from "../OverlayActions";

type Milliseconds = number;

const toMilliseconds = (seconds:number)=> seconds * 1000;

export class Timeline {

    @observable cuePoints: CuePoints = new CuePoints();
    private subscribers: ((cue: Cue) => void)[] = [];
    private currentTime: Milliseconds = 0;
    private updateInterval: Milliseconds = 250;
    private tolerance: Milliseconds = 250;

    private armedCues: Cue[] = [];

    updateTime(time: Milliseconds) {

        const diff = time - this.currentTime;

        if(diff < 0) {
            this.disarmCues();
            this.resetCuesBefore(this.currentTime);
        }

        this.currentTime = time;
        //console.log('timeline currentTime', this.currentTime);
        const cues = this.armAndReturnImmediatelyPendingCues();
        if(cues.length > 0) {
            this.armedCues = this.armedCues.concat(cues);
        }
    }

    pause() {
        this.disarmCues();
    }

    disarmCues() {
        let cue: Cue | undefined;
        while (this.armedCues.length > 0) {
            cue = this.armedCues.shift();
            if (cue) {
                clearTimeout(cue.timeoutId);
                cue.status = CueStatus.Pending;
            }
        }
    }

    play() {
        this.firePendingClearCues();
    }

    subscribe(subscriber: (cue: Cue) => void) {
        if (this.subscribers.indexOf(subscriber) < 0) this.subscribers.push(subscriber);
    }

    unsubscribe(subscriber: (cue: Cue) => void) {
        let idx = this.subscribers.indexOf(subscriber);
        if (idx > -1) this.subscribers.splice(idx, 1);
    }

    addCueByProps<T = any>(time: Milliseconds, action: string, payload?: any): Cue<T> {
        const cue = new Cue(action, time, payload);
        this.cuePoints.addCue(cue);
        return cue;
    }

    addCue<T = any>(cue: Cue): Cue<T> {
        this.cuePoints.addCue(cue);
        return cue;
    }

    addCueWithAutoClear(cue:Cue, duration:number) {
        const clearCue = new Cue(CLEAR, cue.time + duration, {id:cue.id});
        this.cuePoints.addCue(cue);
        this.cuePoints.addCue(clearCue);
    }

    removeCue(cue: Cue) {
        this.cuePoints.remove(cue);
    }

    removeCueById(id: number) {
        this.cuePoints.removeById(id);
    }

    armAndReturnImmediatelyPendingCues() {
        const timeWindowStart = this.currentTime - this.tolerance;
        const timeWindowEnd = this.currentTime + this.updateInterval;
        let cues = this.cuePoints.getPendingCuesBetween(timeWindowStart, timeWindowEnd);
        let i, cue:Cue, relativeCueOffset;
        for(i = 0; i < cues.length; i++) {
            cue = cues[i];
            relativeCueOffset = cue.time - this.currentTime;
            relativeCueOffset = relativeCueOffset <= 0 ? 1 : relativeCueOffset;
            //console.log('relativeCueOffset', relativeCueOffset);
            cue.status = CueStatus.Armed;
            cue.timeoutId = setTimeout(() => {
                //console.log('subscribers', this.subscribers);
                cue.status = CueStatus.Fired;
                this.dispatch(cue);
            }, relativeCueOffset);
        }
        //console.log('armed cues', cues);
        return cues;
    }

    firePendingClearCues() {
        const cues = this.cuePoints.getPendingCuesWithActionBefore(CLEAR, this.currentTime);
        let i, cue;
        for(i = 0; i < cues.length; i++) {
            cue = cues[i];
            cue.status = CueStatus.Fired;
            this.dispatch(cue);
        }
    }

    resetCuesBefore(time:number) {
        const cues = this.cuePoints.getCuesBefore(time);
        cues.forEach(cue=>cue.status = CueStatus.Pending);
    }

    reset() {
        this.currentTime = 0;
        this.cuePoints.cues.forEach(cue=>cue.status = CueStatus.Pending);
    }

    dispatch(cue:Cue) {
        this.subscribers.forEach(fn => fn.call(this, cue));
    }
}