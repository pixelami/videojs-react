import videojs from 'video.js'
import {Timeline} from "./timeline/Timeline";
import {Playlist, PlayListItem} from "./Playlist";
import {computed, observable} from "mobx";
import {OverlayRenderer} from "./OverlayRenderer";

export class VideoPlayerState {

    private player: videojs.Player | any;
    private timeoutWatcherId: any;

    timeline: Timeline;
    playlist: Playlist;
    overlayRenderer: OverlayRenderer;

    contentTimeoutInterval = 10 * 1000;

    @observable private _autoPlay = true;
    @computed get autoPlay() {
        return this._autoPlay
    }

    @observable private _contentReadyToPlay = false;
    @computed get contentReadyToPlay() {
        return this._contentReadyToPlay
    }


    constructor(timeline: Timeline, playlist: Playlist, overlayRenderer: OverlayRenderer) {
        this.timeline = timeline;
        this.playlist = playlist;
        this.overlayRenderer = overlayRenderer;
    }

    /**
     * Called from the VideoPlayer component when the videojs instance is ready.
     * @param player
     */
    setPlayer(player: videojs.Player) {
        this.unsetPlayer();
        this.player = player;

        if (this.player) {

            this.player.on('loadeddata', () => this.onLoadedData());
            this.player.on('loadedmetadata', () => this.onLoadedMetadata());
            this.player.on('timeupdate', () => this.onTimeUpdate());
            this.player.on('pause', () => this.onPause());
            this.player.on('play', () => this.onPlay());
            this.player.on('ended', () => this.onEnded());
            this.player.on('error', () => this.onError());
            this.start();
        }
    }

    private onLoadedData() {
    }

    private onLoadedMetadata() {
        this._contentReadyToPlay = true;
    }

    private onTimeUpdate() {
        if (this.player) {
            const t = this.player.currentTime();
            this.timeline.updateTime(t * 1000);

            if (this.timeoutWatcherId) {
                this.clearTimeoutWatcher(t);
            }
        }
    }

    private onPause() {
        this.timeline.pause();
    }

    private onPlay() {
        //console.log("onPlay");
        this.clearErrors();
        this.initiateTimeoutWatcher();
        if (this.autoPlay) this.player.autoplay(this.autoPlay);
    }

    private onEnded() {
        this.playlist.increment();
        if (this.autoPlay) this.start();
    }

    private onError() {

        const err = this.player.error();
        console.log(err);

        if (this.timeoutWatcherId) this.clearTimeoutWatcher();
        this.showErrorModal();
    }

    private showErrorModal() {

        this.overlayRenderer.executeAction("RENDER", videoErrorOverlayPayload);

        this.player.setTimeout(() => {
            this.clearErrors();
            this.playlist.increment();
            this.start();

        }, videoErrorOverlayPayload.props.duration);
    }

    clearErrors() {
        this.player.error(null);
        this.overlayRenderer.executeAction("CLEAR", {id: videoErrorOverlayPayload.id});
    }

    start() {
        if (this.playlist.current) {
            this.setCurrent(this.playlist.current)
        }
    }

    setCurrent(data: PlayListItem) {
        if (this.player.currentSrc() == data.url) return;
        this.playlist.setCurrent(data);
        const {url, type} = data;
        const srcPayload = {src: url, type, ...data.options};
        console.log('setting player source', srcPayload);
        this.player.src(srcPayload);

        if (this.player.autoplay()) {
            this.initiateTimeoutWatcher();
        }
    }

    private initiateTimeoutWatcher() {

        if (this.timeoutWatcherId) {
            this.clearTimeoutWatcher();
        }

        //console.log('timeout watcher started', time);
        this.timeoutWatcherId = this.player.setTimeout(() => {

            if (this.player.currentTime() === 0) {
                console.warn("content stalled...");
                this.showErrorModal();
                this.clearTimeoutWatcher();
            }
        }, this.contentTimeoutInterval);
    }

    clearTimeoutWatcher(time: any = -1) {
        if (time === 0) return;
        //console.log('timeout watcher cleared', time);
        this.player.clearTimeout(this.timeoutWatcherId);
        this.timeoutWatcherId = null;
    }

    toggleAutoPlay() {
        this._autoPlay = !this._autoPlay;
        this.player.autoplay(this._autoPlay);
    }

    unsetPlayer() {
        if (this.player) this.player.dispose();
    }
}


const videoErrorOverlayPayload = {
    id: "videoError",
    renderer: "videoError",
    props: {
        animation: {enter: "center", exit: "center"},
        horizontalAlign: "center",
        verticalAlign: "top",
        duration: 4000
    }
};