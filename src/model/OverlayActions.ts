import {OverlayRenderer, RenderPayload} from "./OverlayRenderer";

export const RENDER = "RENDER";
export const CLEAR = "CLEAR";

export type ActionType = "RENDER" | "CLEAR" ;

export interface Action<TPayload = any> {
    execute(payload: TPayload, target:OverlayRenderer): any;
}

class RenderAction implements Action<RenderPayload> {

    execute(payload: RenderPayload, target: OverlayRenderer): any {
        const content = target.content;
        const idx = content.findIndex(node => node.id === payload.id);
        if (idx < 0) {
            // decorate the payload.props that will be used by the renderer
            payload.props.active = true;
            payload.props.id = payload.id;
            payload.props.onAnimationEnd = null;
            payload.props.deactivate = function() {
                map[CLEAR].execute(payload, target);
            };
            payload.props.activate = () => payload.props.active = true;

            content.push(payload);
        } else {
            console.warn(`Render action aborted. '${payload.id}' is already active`)
        }
    }
}

class ClearAction implements Action<{ id: string }> {

    execute(payload: { id: string }, target: OverlayRenderer): any {
        const content = target.content;
        const idx = content.findIndex(node => node.id === payload.id);
        if (idx > -1) {
            const payload = content[idx];
            payload.props.active = false;
            payload.props.onAnimationEnd = (evt: any) => {
                content.splice(idx, 1);
            }
        }
    }
}

export const map: {[key in ActionType]:Action} = {
    [CLEAR]: new ClearAction(),
    [RENDER]: new RenderAction(),
};