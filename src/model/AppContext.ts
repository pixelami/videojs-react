import {Timeline} from "./timeline/Timeline";
import {OverlayRenderer} from "./OverlayRenderer";
import {VideoPlayerState} from "./VideoPlayerState";
import {Playlist} from "./Playlist";
import {Overlays} from "./Overlays";


export class AppContext {

    timeline = new Timeline();
    overlayRenderer = new OverlayRenderer();
    overlay = new Overlays();
    playlist = new Playlist();
    playerState = new VideoPlayerState(this.timeline, this.playlist, this.overlayRenderer);

    constructor() {
        this.timeline.subscribe(cue=> this.overlayRenderer.processCue(cue));
        this.playlist.initialize()
    }
}