interface OverlayData {
    images: OverlayAction[];
    text: OverlayAction[];
    html: OverlayAction[];
    iframes: OverlayAction[];
}

interface OverlayAction {
    action: string;
    payload: OverlayActionPayload;
}

type RendererType = "image" | "text" | "html" | "iframe";

interface OverlayActionPayload {
    id: string;
    title: string;
    renderer: RendererType;
    props: OverlayActionPayloadProps;
}

type VerticalAlignType = "top" | "bottom";
type HorizontalAlignType = "left" | "center" | "right";

interface OverlayActionPayloadProps {
    [key: string]: any;
    animation: OverlayAnimation;
    verticalAlign: VerticalAlignType;
    horizontalAlign: HorizontalAlignType;
}

type OverlayAnimationType = HorizontalAlignType;

interface OverlayAnimation {
    enter: OverlayAnimationType;
    exit: OverlayAnimationType;
}

const data: OverlayData = {
    images: [
        {
            action: 'RENDER',
            payload: {
                title: "Unicorn",
                renderer: 'image',
                id: 'unicorn',
                props: {
                    url: './resources/unicorn.png',
                    animation: {
                        enter: 'center',
                        exit: 'center'
                    },
                    verticalAlign: 'top',
                    horizontalAlign: 'right',
                    clickUrl: "https://en.wikipedia.org/wiki/List_of_unicorns"
                }
            }
        },
        {
            action: 'RENDER',
            payload: {
                title: "Test Drive 01",
                renderer: 'image',
                id: 'car1',
                props: {
                    url: './resources/car-sticker-01.png',
                    animation: {
                        enter: 'center',
                        exit: 'center'
                    },
                    verticalAlign: 'bottom',
                    horizontalAlign: 'left',
                    clickUrl: "https://en.wikipedia.org/wiki/Test_Drive_(series)"
                }
            }
        },
        {
            action: 'RENDER',
            payload: {
                title: "Test Drive 02",
                renderer: 'image',
                id: 'car2',
                props: {
                    url: './resources/car-sticker-02.png',
                    animation: {
                        enter: 'right',
                        exit: 'left'
                    },
                    verticalAlign: 'bottom',
                    horizontalAlign: 'right',
                    clickUrl: "https://en.wikipedia.org/wiki/Test_Drive_(series)"
                }
            }
        }
    ],
    text: [
        {
            action: 'RENDER',
            payload: {
                title: "Text",
                renderer: 'text',
                id: 'text1',
                props: {
                    text: 'Sample caption',
                    color: '#e9e9e9',
                    animation: {
                        enter: 'center',
                        exit: 'center'
                    },
                    verticalAlign: 'bottom',
                    horizontalAlign: 'left',
                    clickUrl: "https://en.wikipedia.org/wiki/List_of_unicorns"
                }
            }
        }
    ],
    html: [
        {
            action: 'RENDER',
            payload: {
                title: "HMTL",
                renderer: 'html',
                id: 'html1',
                props: {
                    html: "<div><h5>Sample HTML Overlay</h5></div>",
                    animation: {
                        enter: 'center',
                        exit: 'center'
                    },
                    verticalAlign: 'bottom',
                    horizontalAlign: 'left',
                }
            }
        }
    ],
    iframes: [
        {
            action: 'RENDER',
            payload: {
                title: "Book Test Drive 01",
                renderer: 'iframe',
                id: 'test-drive-01',
                props: {
                    url: "./resources/overlay-01.html",
                    animation: {
                        enter: 'center',
                        exit: 'center'
                    },
                    verticalAlign: 'bottom',
                    horizontalAlign: 'left',
                    clickUrl: "https://en.wikipedia.org/wiki/Test_Drive_(series)"
                }
            }
        },
        {
            action: 'RENDER',
            payload: {
                title: "Book Test Drive 02",
                renderer: 'iframe',
                id: 'test-drive-02',
                props: {
                    url: "./resources/overlay-02.html",
                    animation: {
                        enter: 'left',
                        exit: 'right'
                    },
                    verticalAlign: 'bottom',
                    horizontalAlign: 'left',
                    clickUrl: "https://en.wikipedia.org/wiki/Test_Drive_(series)"
                }
            }
        }
    ]
};

export default data;