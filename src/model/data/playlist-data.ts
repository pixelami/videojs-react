interface PlaylistDataCollection {
    playlists:PlaylistData[];
}

interface PlaylistData {
    title:string;
    subtitle:string;
    thumb:string;
    url:string;
    type:string;
    options:any;
}

export default {
    playlists:[
        {
            title:"Parkour",
            subtitle:"travel | sports",
            thumb:"",
            url:"https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8",
            type:"application/x-mpegURL",
            options:{}
        },
        {
            title:"Blender Foundation",
            subtitle:"animation",
            thumb:"",
            url:"https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8",
            type:"application/x-mpegURL",
            options:{}
        },
        {
            title:"Table Ronde",
            subtitle:"TeleQuebec",
            thumb:"",
            url:"https://mnmedias.api.telequebec.tv/m3u8/29880.m3u8",
            type:"application/x-mpegURL",
            options:{}
        },
        {
            title:"Big Buck Bunny",
            subtitle:"nature documentary",
            thumb:"",
            url:"http://184.72.239.149/vod/smil:BigBuckBunny.smil/playlist.m3u8",
            type:"application/x-mpegURL",
            options:{}
        },
        {
            title:"CORS Error Test",
            subtitle:"handle content errors??",
            thumb:"",
            url:"http://www.streambox.fr/playlists/test_001/stream.m3u8",
            type:"application/x-mpegURL",
            options:{
                withCredentials:true
            }
        },
        {
            title:"Bip Bop",
            subtitle:"test content",
            thumb:"",
            url:"https://d2zihajmogu5jn.cloudfront.net/bipbop-advanced/bipbop_16x9_variant.m3u8",
            type:"application/x-mpegURL",
            options:{}
        },
        {
            title:"Feelings",
            subtitle:"webm",
            thumb:"",
            url:"http://yt-dash-mse-test.commondatastorage.googleapis.com/media/feelings_vp9-20130806-244.webm",
            type:"video/webm",
            options:{}
        },
        {
            title:"Feelings (Error 1)",
            subtitle:"mpeg-dash manifest",
            thumb:"",
            url:"http://yt-dash-mse-test.commondatastorage.googleapis.com/media/feelings_vp9-20130806-manifest.mpd",
            type:"application/dash+xml",
            options:{}
        },
        {
            title:"Webm enc Example (Error 2)",
            subtitle:"webm encrypted",
            thumb:"",
            url:"https://simpl.info/eme/video/Chrome_44-enc_av.webm",
            type:"video/webm",
            options:{}
        },
        {
            title:"กระต่ายฟันใหญ่",
            subtitle:"การใคร่ครวญของจักรวาล",
            thumb:"",
            url:"https://s3.amazonaws.com/ds-video-public/test/bbb/demo/h264.mpd",
            type:"application/dash+xml",
            options:{}
        },
        {
            title:"Elephants Dream",
            subtitle:"test encode from BBC R&D",
            thumb:"",
            url:"http://rdmedia.bbc.co.uk/dash/ondemand/elephants_dream/1/client_manifest-all.mpd",
            type:"application/dash+xml",
            options:{}
        },
        {
            title:"Forest Poem",
            subtitle:"dual audio (en/de)",
            thumb:"",
            url:"https://dash.akamaized.net/dash264/TestCases/10a/1/iis_forest_short_poem_multi_lang_480p_single_adapt_aaclc_sidx.mpd",
            type:"application/dash+xml",
            options:{}
        },
        {
            title:"Tears of Steel",
            subtitle:"Blender test content",
            thumb:"",
            url:"http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/TearsOfSteel/1sec/TearsOfSteel_1s_onDemand_2014_05_09.mpd",
            type:"application/dash+xml",
            options:{}
        }
    ]
} as PlaylistDataCollection;