import {Cue} from "./timeline/CuePoints";
import {observable} from "mobx";
import {map, ActionType} from "./OverlayActions";

export interface RendererProps {
    [key:string]:any;
    active:boolean;
    onAnimatedEnded:()=>void;
    deactivate:()=>void;
}

export interface RenderPayload {
    renderer: string;
    id: string;
    props: RendererProps;
}

export interface ClearPayload {
    id: string;
}

export interface RendererDescriptor {
    component: any;
    props: RendererProps;
    id: string;
}

export class OverlayRenderer {

    @observable content:RenderPayload[] = [];

    processCue(cue:Cue) {
        console.log('processing cue', cue);
        this.executeAction(cue.action as ActionType, cue.payload);
    }

    executeAction(actionType:ActionType, payload:RenderPayload|ClearPayload) {
        const action = map[actionType];
        if(action) {
            action.execute(payload, this);
        } else {
            console.error('Unknown action type', actionType)
        }
    }
}

