import {computed, observable} from "mobx";
import playlist_data from "./data/playlist-data";

export interface PlayListItem {
    url: string;
    type: string;
    thumb: string;
    title: string;
    subtitle: string;
    options: any;
}

export class Playlist {

    @observable items: PlayListItem[] = [];

    @observable index:number = 0;

    @computed get current(): any {
        return this.items[this.index];
    }

    initialize() {
        this.items.push(...playlist_data.playlists);
    }

    increment() {
        let nextIndex = this.index + 1;
        if(nextIndex >= this.items.length) nextIndex = 0;
        this.index = nextIndex;
    }

    setCurrent(data: PlayListItem) {

        if(data == this.current) return;

        const idx = this.items.findIndex(item => item.url == data.url);
        if(idx < 0) {
            console.error('unable to find index of ',data);
            return;
        }
        this.index = idx;
    }
}